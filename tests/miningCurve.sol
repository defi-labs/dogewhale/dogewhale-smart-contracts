// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

contract calctests {

    function sub(int256 a, int256 b) internal pure returns (int256) {
        int256 c = a - b;
        return c;
    }

    function mul(int256 a, int256 b) internal pure returns (int256) {
        return a * b;
    }

    function div(int256 a, int256 b) internal pure returns (int256) {
        return a / b;
    }

    function sqrt(uint x) internal pure returns (uint y) {
        uint z = (x + 1) / 2;
        y = x;
        while (z < y) {
            y = z;
            z = (x / z + z) / 2;
        }
    }

    function calcTime(int256 _userTime) public pure returns (int256, int256, uint256, uint256) {
        int256 maxPct = 50;
        int256 inflection = 200;
        uint256 steep = 500;

        int256 XminusB = sub(_userTime, inflection);
        int256 over = mul(maxPct, XminusB);
        uint256 under = uint256((XminusB)**2) + steep;
        uint256 total = uint(div(over, int256(sqrt(under))) + maxPct);
        return (XminusB, over, under, total); //4 vars output for function debugging
    }
}
