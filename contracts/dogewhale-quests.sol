// SPDX-License-Identifier: MIT

/*                                                                                                                                  
                                                                   +:                                                             
                                                                 -=++.                                                            
                                                                 =++++:                                                           
                                                                :+==+*.                                                           
                                                                :++=++.                                                           
                                                                ===++=                                                            
                                                                .++++-                                                            
                                                                -===++                                                            
                                                                .++=+=                                                            
                                                                .+=+=+                                                            
                                                                 :+=+=                                                            
                                                                  ++=+                                                            
                                                                 ===+.                                                            
                                                                 -==+=                                                            
                                                                 -==+:                                                            
                                                                 -==+:                                                            
                                                                 -==+.                                                            
                                                                 .==+:                         .:.                                
                                                                 ==+=.                          -::                               
                                                                 -==+.                         :::-:                              
                                                                 -==+= .                      ::::::-:.                           
                                                                 :=++-=+.                    :::::::::--                          
                                                                ==+++==+                    ::::::::::::-                         
                                                                +--==++.                    .::::::::::-: .::----------           
                                                                -=+==+=.                     :::::::--=::-:::::::::---:           
                                                               .: :::. :                      ::::::::-=::::::::::--              
                                   ............                .. .    ...                     .:::::::::::::::::-=               
                            ........................           :. .     : .                     ::::::::::::::::=-                
                        .... .   ...           ...  ...        :. .     .:.                   .:::::::::-:::---:                  
                    .......  ..  .........     ... .   ..      -..:.     :.:                .:::::::::::=.::.                     
                  ... ....   ..........    ...  . ..    .:    =-..-=-.  .: .             .::::::::::::::=                         
               ...... ..           ....   ..  ..  . .   ..:  -==:.:-==-::...         ..:::::::::::::::::+                         
              ...  ...         ........    .......   .... :  ++-....:===..    ....::::::::::::::::::::::+                         
            ... ... .  .. .....  ...................    ..::-++:     .===:::::::::::::::::::::::::::::::+                         
            .. ...  .. ......   .....::::::=*++=::::::.::::::::::::::::===::::::::::::::::::::::::::::::=                         
           .  .   .   ....   .  .. .::::::::+*+*-:::::::::::::::::::::::===::::::::::::::::::::::::::::--                         
           :  ........      .... ...-::::::::**+*:::::::::::::::::::::::-+=-:::::::::::::::::::::::::::=:                         
           :  ..    ......  ......:=**=-:::::=*+*-:::::::::::::::::::::::=+=:::::::::::::::::::::::::::+                          
           . ... .............::::-=**+++-::::*++-::::::::::::::::::::::::++::::::::::::::::::::::::::-=                          
           .. . ... ...:--==-::::::::-+*+++-::*+*-::::::::::::::::::::::::=+=:::::::::::::::::::::::::=.                          
            ........:-=-::--=--:::::::::=*+++=*+*:::::::::::::::::::::::::-++:::::::::::::::::::::::::=                           
            ......:=---. .:=+---:::::::.  -**+++=-:::::::::::::::::::::::::++-:::::::::::::::::::::::+.                           
             ...:=-=-:-- .==+=-=::::::.    ===+*+==::::::::::::::::::::::::===:::::::::::::::::::::.-:                            
              .:::=-=:---:-+=---::::.      -==+++==::::::::::::::::::::::::===:::::::::::::::::::. :-                             
                 :-=-=---=----::::..        -+++*=:::::::::::::::::::::::::===:::::::::::::::::.  :=                              
              :+*+..:------:.....:++:..      =*+*=:::::::::::::::::::::::::==+::::::::::::::..   -=                               
              *##*+ .   ...::::-=##*..       **+*::::::::::::::::::::::::::==+:::::::::::..     =-                                
              :%@#*....:--:--==*%%*::..     :*++=::::::::::::::::::::::::::+==::::::::..      .=:                                 
               :*#*........:-+#%#=-:.      :*++=:::::::::::::::::::::::::::+==::::..         :=.                                  
                 :*-===++*####+-..   ..  .=*++-.:::-::::::::::::::::::::::-++:..           .=-                                    
                  :*#####*+=:..        :=*+++. :--::::::::::-::::::...... =+=             :=.                                     
                    -+=...          :-+*+++: .--::::::::::::=.           :++-           :=:                                       
                      :=.       .:=+****=:  :=::::::::::::::=            +++         .:-:                                         
                        -=::-=+*****+-:    --::::::::::::::=.           =++:      .:--.                                           
                         -******=-:       :=:::::::::::::=-           .+++-    .-=-.                                              
                           .:=-.          +:::::::::--=-:            -+=+: .:--:                                                  
                               :---.     .+:::::::=-..            .-++=+=--.                                                      
                                   .---:. +::::::=-           .:-++++=:                                                           
                                       .:--=:::::+-::::-----+*+===-:.                                                             
                                           :=:::-+..                                                                              
                                             -===.                                                                                

                                             DOGEWHALE QUESTS

by DeFi Labs
*/

pragma solidity ^0.8.0;

abstract contract Initializable {
    bool private _initialized;
    bool private _initializing;
    modifier initializer() {
        require(_initializing || !_initialized, "Initializable: contract is already initialized");
        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }
}

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

interface IUniswap {
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline)
        external
        returns (uint[] memory amounts);
    function WETH() external pure returns (address);
    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) external pure returns (uint amountOut);
    function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
    function addLiquidity(address tokenA, address tokenB, uint amountADesired, uint amountBDesired, uint amountAMin, uint amountBMin, address to, uint deadline) external returns (uint amountA, uint amountB, uint liquidity);
    function removeLiquidity(address tokenA, address tokenB, uint liquidity, uint amountAMin, uint amountBMin, address to, uint deadline) external returns (uint amountA, uint amountB);
    //Factory
    function getPair(address tokenA, address tokenB) external view returns (address pair);
}

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract DogewhaleQuests is Context, Initializable {
    address public _owner;
    address public dogewhale;

    address private pancakefactory;
    address private pancakeswapRouter;
    address private apefactory;
    address private apeswapRouter;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _walletOwner, address indexed _spenderAddy, uint256 _value);

    modifier onlyOwner() {
        require(_owner == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    function init() external initializer {
        _owner = 0x16f7c37FF84a71a8be7EB24228Cdc8E438D12060;
        dogewhale = 0x43adC41cf63666EBB1938B11256f0ea3f16e6932;
        pancakefactory = 0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73;
        pancakeswapRouter = 0x10ED43C718714eb63d5aA57B78B54704E256024E;
        apefactory = 0x0841BD0B734E4F5853f0dD8d7Ea041c241fb0Da6;
        apeswapRouter = 0xcF0feBd3f17CEf5b47b0cD257aCf6025c5BFf3b7;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    // BASIC VIEW FUNCTIONS  =======================================================================>
    /////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////
    // QUEST REWARDS FUNTIONS  =====================================================================>
    /////////////////////////////////////////////////////////////////////////////////////////////////
    /*
        Dogewhale quests will be designed to incentivize value accruing behaviour and reward holders
        whilst having fun. Dogewhale is in Zurbitos, trying his best to make his way back home.

        Development ongoing
    */

    // this function was added to unlock the quest funds for
    // bridging into Polygon Blockchain for mining rewards
    function miningRewardsToPolygon(uint256 _amount) public virtual returns (bool)  {
        require(msg.sender == _owner, "unable");
        require(_amount != 0, "unable");
        IERC20(dogewhale).transfer(_owner, _amount);
        return true;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    // MATH FUNCTIONS  =============================================================================>
    /////////////////////////////////////////////////////////////////////////////////////////////////
    function _pct(uint _value, uint _percentageOf) internal virtual returns (uint256 res) {
        res = (_value * _percentageOf) / 10 ** 18;
    }
}
