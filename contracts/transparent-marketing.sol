// SPDX-License-Identifier: MIT

/*
                                                                      :---------::.
                                                                    -=-:::::::::::---:
                                                    .:------::.   .=-:::::::::::::::::=-
                                                 :=--::::::::-----=-:::::::::::::::::::-=
                                              .-=:::::::::::::::::::::::::::::::::::::::-=
                                             -=::::::::::::::::::::::::::::::::::::::::::=.
                                            =-::::::::::::::::::::::::::::::::::::::::::::----::::.
                                           --:::::::::::::::::::::::::::::::::::::::::::::::::::::---.
                                           +:::::::::::::::::::::::::::::::::::::::::::::::::::::::::-:
                                           =-:::::::::::::::::::::::::::::::::::::::::::::::::::::::::=
                                           :=:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                                            -=:::::::::::::::::::::::::::::::::::::::::::::::::::::::::-
                                        .--=--::::::::::::::::::::::::::::::::::::::::::::::::::::::::-:
                                      :=-:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::-
                                     =-::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::-
                                    .=::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                                    .=:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
                                     =-:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::-
                                     .=::::::::::::::::::::::::-::::::::::::::::::::::::::::::::::::::-
                                      .=:::::::::::::::::::::::-::::::::::::::::::::::::::::::::::::::.
                                        --:::::::::::::::::::::-:::::-:::-:::::::::::::::::::::::::::
                                         .:--+:::::::::::::::::-:--:::---:=-::::::::::::::::::::..
                                            =::::::::::::::::::-:--::::-::-=::::::::::::::..:::::.
                              :-.          =:::::::::::::::::-=-::---:::::=-::::.   .::::. .-=---:-:.
                            .=-:=.         =:::::::::::::::::---::::::::::+:::::::.. .:::::+*+-:....-.
                           -=:::--         :-::::::::::::::::::=:::::::::-=::-===#*=----::+-:+=..::::
            .=----:::...:-=-::::=:          .--::::::::::::::---:::::::::+----:-++:.=..-=-:=-----:   :
             +-::::::::::::::::=-          .::.::-=++===++=---:::::::::::-......-+=-::-=-:...  ..     .
              =-:::::::::::::-=.          -#****##********+:::::::::....:---:-----==--::.  :=*###*. ::--::::.
               :=-::::::::::==-----------+#*+**************-:::::::       ..:::::::::...   +#@@#@%::-:--::::.
                 .:---=--:::::::::::::::-#*+******#********+::::::.           .:------:::...=*##*-.::::-:-:
                        =:::::::::::::::#*+********#********-::::::          ..  :-----=-:.:...--=*+.  :   :.
                        .+:::::::::::::***+*********#*******+::::::            :-. .==+---==+*##*=:.   :
                         -=:::::::::::-#*************#*******-:::::.          .  .-: .:-=====-::...    +:
                          =-::::::::::****************#*******::::::.           ..         ...        .*+
                           =-::::::::-#****************#******+::::::.                                =**.
                            -=:::::::=******************#******+::::::.                              -***:
                             :=::::::+*******************#*******=:::::.                           :+****
                              .=:::::=********************#********+=-.                       .:-=******-
                                =-:::-#*********************#************++============+==:.+**********:
                                 :=..:#***********************###*****************+++****++=-=+******=
                                   --.+**********#****************####******************=--:=-:+***-
                                     -+#********#*************#*********####************-:-=:-=+=:
                                       ********#**+************#****************#########*=-==:
                                       :#******#***************##********************###+-----
                                        *******#****************#*****************#*+-.   =---:
                                        .:-=+*##****************#************#*=-:        --:-=.
                                               +****************#**########*:             -----=
                                               :#***************#-=***+++-=:              -====-:
                                                :=+###******##**-   :=+===:               =----==
                                                   --:------::-:                         :====--=
                                                    .-::::::::-                          =-----==.
                                                      ::::::::                          -----=--=:
                                                                                       -===----=+.
                                                                                        =------=:
                                                                                        .=---==.
                                                                                          --:.
                                                    TRANSPARENT MARKETING

by DeFi LABS
*/

pragma solidity ^0.8.0;

abstract contract Initializable {
    bool private _initialized;
    bool private _initializing;
    modifier initializer() {
        require(_initializing || !_initialized, "Initializable: contract is already initialized");
        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }

        _;

        if (isTopLevelCall) {
            _initializing = false;
        }
    }
}

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

interface IUniswap {
    function swapExactTokensForTokens(
        uint amountIn,
        uint amountOutMin,
        address[] calldata path,
        address to,
        uint deadline)
        external
        returns (uint[] memory amounts);
    function WETH() external pure returns (address);
    function getAmountOut(uint amountIn, uint reserveIn, uint reserveOut) external pure returns (uint amountOut);
    function getReserves() external view returns (uint112 reserve0, uint112 reserve1, uint32 blockTimestampLast);
    function addLiquidity(address tokenA, address tokenB, uint amountADesired, uint amountBDesired, uint amountAMin, uint amountBMin, address to, uint deadline) external returns (uint amountA, uint amountB, uint liquidity);
    function removeLiquidity(address tokenA, address tokenB, uint liquidity, uint amountAMin, uint amountBMin, address to, uint deadline) external returns (uint amountA, uint amountB);
    //Factory
    function getPair(address tokenA, address tokenB) external view returns (address pair);
}

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract TransparentMarketing is Context, Initializable {
    address public _owner;
    mapping(address => bool) public supervisors;

    address public dogewhale;
    address public marketingAddy;

    //marketing tx register
    uint256 private howManyTxs;
    mapping(uint256 => address) private toWhom;
    mapping(uint256 => uint256) private howMuch;
    mapping(uint256 => string) private purpose;
    mapping(uint256 => string) private comment;
    mapping(uint256 => string) private social;
    mapping(uint256 => bool) private inBNB;

    address private pancakefactory;
    address private pancakeswapRouter;
    address private apefactory;
    address private apeswapRouter;

    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _walletOwner, address indexed _spenderAddy, uint256 _value);

    modifier onlyOwner() {
        require(_owner == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    function init() external initializer {
        _owner = 0x16f7c37FF84a71a8be7EB24228Cdc8E438D12060;
        supervisors[_owner] = true;
        dogewhale = 0x43adC41cf63666EBB1938B11256f0ea3f16e6932;
        marketingAddy = _owner;
        howManyTxs = 0;
        pancakefactory = 0xcA143Ce32Fe78f1f7019d7d551a6402fC5350c73;
        pancakeswapRouter = 0x10ED43C718714eb63d5aA57B78B54704E256024E;
        apefactory = 0x0841BD0B734E4F5853f0dD8d7Ea041c241fb0Da6;
        apeswapRouter = 0xcF0feBd3f17CEf5b47b0cD257aCf6025c5BFf3b7;
    }

    //admin funcs
    function toggleSupervisor(address _addy) public virtual onlyOwner returns (bool) {
        require(_addy != _owner, "Not Allowed");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");

        if (supervisors[_addy] == true) {
            supervisors[_addy] = false;
        } else {
            supervisors[_addy] = true;
        }
        return true;
    }

    function setMarketingAddy(address _addy) public virtual onlyOwner returns (bool) {
        marketingAddy = _addy;
        return true;
    }

    //view funcs
    function viewNumberOfTxs() public view returns (uint256) {
        return howManyTxs;
    }

    function viewMarketingTx(uint256 _whichTx) public view returns (uint256, address, uint256, string memory, string memory, string memory, bool) {
        return (_whichTx, toWhom[_whichTx], howMuch[_whichTx], purpose[_whichTx], comment[_whichTx], social[_whichTx], inBNB[_whichTx]);
    }

    //release funds for marketing
    function MarketingTransactionInDogeWhale(address _to, uint256 _amount, string memory _purpose, string memory _comment, string memory _social) public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not permited");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");
        IERC20(dogewhale).transfer(_to, _amount);

        toWhom[howManyTxs] = _to;
        howMuch[howManyTxs] = _amount;
        purpose[howManyTxs] = _purpose;
        comment[howManyTxs] = _comment;
        social[howManyTxs] = _social;
        inBNB[howManyTxs] = false;

        howManyTxs += 1;
        return true;
    }

    function MarketingTransactionInBNB_pancakeswap(address _to, uint256 _amount, string memory _purpose, string memory _comment, string memory _social) public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not permited");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");

        address[] memory path = new address[](2);
        path[0] = dogewhale;
        path[1] = IUniswap(pancakeswapRouter).WETH();

        address LP = IUniswap(pancakefactory).getPair(path[0], path[1]);

        uint amountIn = _amount;

        uint amountOut = IUniswap(pancakeswapRouter).getAmountOut(amountIn, IERC20(path[0]).balanceOf(LP), IERC20(path[1]).balanceOf(LP));
        uint amountOutMin = amountOut - _pct(amountOut, 1*10**16);

        IERC20(path[0]).approve(pancakeswapRouter, amountIn);
        IUniswap(pancakeswapRouter).swapExactTokensForTokens(amountIn, amountOutMin, path, _to, block.timestamp+120);

        toWhom[howManyTxs] = _to;
        howMuch[howManyTxs] = _amount;
        purpose[howManyTxs] = _purpose;
        comment[howManyTxs] = _comment;
        social[howManyTxs] = _social;
        inBNB[howManyTxs] = true;

        howManyTxs += 1;
        return true;
    }

    function MarketingTransactionInBUSD_pancakeswap(address _to, uint256 _amount, string memory _purpose, string memory _comment, string memory _social) public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not permited");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");

        address[] memory path = new address[](2);
        path[0] = dogewhale;
        path[1] = IUniswap(pancakeswapRouter).WETH();

        address LP = IUniswap(pancakefactory).getPair(path[0], path[1]);

        uint amountIn = _amount;

        uint amountOut = IUniswap(pancakeswapRouter).getAmountOut(amountIn, IERC20(path[0]).balanceOf(LP), IERC20(path[1]).balanceOf(LP));
        uint amountOutMin = amountOut - _pct(amountOut, 1*10**16);

        IERC20(path[0]).approve(pancakeswapRouter, amountIn);
        IUniswap(pancakeswapRouter).swapExactTokensForTokens(amountIn, amountOutMin, path, address(this), block.timestamp+120);

        //swap to busd
        //underdevelopment

        toWhom[howManyTxs] = _to;
        howMuch[howManyTxs] = _amount;
        purpose[howManyTxs] = _purpose;
        comment[howManyTxs] = _comment;
        social[howManyTxs] = _social;
        inBNB[howManyTxs] = true;

        howManyTxs += 1;
        return true;
    }

    function MarketingTransactionInBNB_apeswap(address _to, uint256 _amount, string memory _purpose, string memory _comment, string memory _social) public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not permited");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");

        address[] memory path = new address[](2);
        path[0] = dogewhale;
        path[1] = IUniswap(apeswapRouter).WETH();

        address LP = IUniswap(apefactory).getPair(path[0], path[1]);

        uint amountIn = _amount;

        uint amountOut = IUniswap(apeswapRouter).getAmountOut(amountIn, IERC20(path[0]).balanceOf(LP), IERC20(path[1]).balanceOf(LP));
        uint amountOutMin = amountOut - _pct(amountOut, 1*10**16);

        IERC20(path[0]).approve(apeswapRouter, amountIn);
        IUniswap(apeswapRouter).swapExactTokensForTokens(amountIn, amountOutMin, path, _to, block.timestamp+120);

        toWhom[howManyTxs] = _to;
        howMuch[howManyTxs] = _amount;
        purpose[howManyTxs] = _purpose;
        comment[howManyTxs] = _comment;
        social[howManyTxs] = _social;
        inBNB[howManyTxs] = true;

        howManyTxs += 1;
        return true;
    }

    //math funcs
    function _pct(uint _value, uint _percentageOf) internal virtual returns (uint256 res) {
        res = (_value * _percentageOf) / 10 ** 18;
    }
}
