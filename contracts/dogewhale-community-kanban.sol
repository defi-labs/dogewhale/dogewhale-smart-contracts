// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

contract CommunityKanBan is Context {
    address public _owner;
    mapping(address => bool) public supervisors;
    
    //gig supervision
    uint256 public numberGigs;
    mapping(uint256 => string) private gigTitle;
    mapping(uint256 => string) private gigBriefing;
    mapping(uint256 => uint256) private payout;
    mapping(uint256 => bool) private isDone;
    mapping(uint256 => address) private completer;
    
    //gig submission
    mapping(uint256 => uint256) public numOfSubmissions;                            // numOfSubmissions[gigID]
    mapping(uint256 => mapping(uint256 => address)) private addressOfSubmitter;
    mapping(uint256 => mapping(uint256 => string)) private twitterHandle;           // twitterHandle[gigID][numOfSubmissions]
    mapping(uint256 => mapping(uint256 => string)) private telegramHandle;
    mapping(uint256 => mapping(uint256 => string)) private linkToAttachments;
    mapping(uint256 => mapping(uint256 => string)) private comments;
    
    address public dogewhale;
    address public marketingAddy;
    uint256 public bnbGwei;
    
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _walletOwner, address indexed _spenderAddy, uint256 _value);
    
    event gigPosted(uint256 _gigIdent);
    event gigClosed(uint256 _gigIdent, uint256 _numSubmission, address indexed _completerPerson);
    event gigSubmited(uint256 _gigIdent, uint256 _numSubmission, address indexed _submitter);
    
    modifier onlyOwner() {
        require(_owner == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    constructor() payable {
        _owner = _msgSender();
        supervisors[_owner] = true;
        numberGigs = 0;
        dogewhale = 0x43adC41cf63666EBB1938B11256f0ea3f16e6932;
        marketingAddy = _owner;
        bnbGwei = 7000000;
    }
    
    //view funcs
    function viewGig(uint256 _gigID) public view returns (uint256, string memory, string memory, uint256, bool, address) {
        return (_gigID, gigTitle[_gigID], gigBriefing[_gigID], payout[_gigID], isDone[_gigID], completer[_gigID]);
    }
    
    function howManySubmittions(uint256 _gigID) public view returns (uint256) {
        return numOfSubmissions[_gigID];
    }
    
    function viewSubmission(uint256 _gigID, uint256 _xSubmission) public view returns (address, string memory, string memory, string memory, string memory) {
        return (addressOfSubmitter[_gigID][_xSubmission], twitterHandle[_gigID][_xSubmission], telegramHandle[_gigID][_xSubmission], linkToAttachments[_gigID][_xSubmission], comments[_gigID][_xSubmission]);
    }
    
    //admin funcs
    function toggleSupervisor(address _addy) public virtual onlyOwner returns (bool) {
        require(_addy != _owner, "Not Allowed");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");
        
        if (supervisors[_addy] == true) {
            supervisors[_addy] = false;
        } else {
            supervisors[_addy] = true;
        }

        return true;
    }
    
    function setMarketingAddy(address _addy) public virtual onlyOwner returns (bool) {
        marketingAddy = _addy;
        return true;
    }
    
    function setHowMuchBNBToSubmit(uint256 _bnbInGwei) public virtual onlyOwner returns (bool) {
        bnbGwei = _bnbInGwei;
        return true;
    }
    
    function postGig(string memory _title, string memory _gigBriefing, uint256 _pay) public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not Allowed");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");
        
        gigTitle[numberGigs] = _title;
        gigBriefing[numberGigs] = _gigBriefing;
        payout[numberGigs] = _pay;
        emit gigPosted(numberGigs);
        
        numberGigs += 1;
        return true;
    }
    
    function closeGig(uint256 _gigID, uint256 _xSubmission, uint256 payoutOverride) public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not Allowed");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");
        
        if (payout[_gigID] == 0) {
            require(payoutOverride != 0, "No zero pay for werk");
            IERC20(dogewhale).transfer(addressOfSubmitter[_gigID][_xSubmission], payoutOverride);
        } else {
            IERC20(dogewhale).transfer(addressOfSubmitter[_gigID][_xSubmission], payout[_gigID]);
        }
        
        isDone[_gigID] = true;
        completer[_gigID] = addressOfSubmitter[_gigID][_xSubmission];
        
        emit gigClosed(_gigID, _xSubmission, completer[_gigID]);
        return true;
    }
    
    //user funcs
    function submitGig(uint256 _gigID, string memory _twitterHandle, string memory _telegramHandle, string memory _linkToAttachments, string memory _comments) public virtual payable returns (bool) {
        require(isDone[_gigID] == false, "Gig is completed");
        require(msg.value >= 7000000 gwei, "BNB required to submit");
        
        addressOfSubmitter[_gigID][numOfSubmissions[_gigID]] = _msgSender();
        twitterHandle[_gigID][numOfSubmissions[_gigID]] = _twitterHandle;
        telegramHandle[_gigID][numOfSubmissions[_gigID]] = _telegramHandle;
        linkToAttachments[_gigID][numOfSubmissions[_gigID]] = _linkToAttachments;
        comments[_gigID][numOfSubmissions[_gigID]] = _comments;
        
        emit gigSubmited(_gigID, numOfSubmissions[_gigID], _msgSender());
        
        numOfSubmissions[_gigID] += 1;
        return true;
    }
    
    //clear contract
    function sendToMarketing() public virtual returns (bool) {
        require(supervisors[_msgSender()] == true, "Not Allowed");
        require(_msgSender() != address(0), "No funny stuff from the zero addy");
        IERC20(dogewhale).transfer(marketingAddy, IERC20(dogewhale).balanceOf(address(this)));
        return true;
    }
}